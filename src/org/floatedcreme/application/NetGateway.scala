package org.floatedcreme.application

import scala.collection.mutable
import java.net.ServerSocket
import java.net.InetSocketAddress

/**
 * Created by nidev on 2015-01-08.
 */
class NetGateway (listening : String, port : Integer) {
  var socket = new ServerSocket()
  socket.bind(new InetSocketAddress(listening, port))
  var sessionId : Int = 1
  var pool = new mutable.Queue[Session]()

  def loop() : Unit = {
    println("서버 가동 중")
    while (true) {
      var accepted_sock = socket.accept
      sessionId += 1
      println("%s로 부터 연결. 세션 : %d".format(accepted_sock.getInetAddress.getHostName, sessionId))

      // TODO: Session Pool 을 만들고, 쓰레드로 각각을 독립적으로 돌릴 걸
      var new_session = new Session(sessionId, accepted_sock)
      new_session.start()
      pool.enqueue(new_session)
    }
  }


}
