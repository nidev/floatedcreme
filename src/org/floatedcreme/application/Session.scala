package org.floatedcreme.application

import java.io.{BufferedInputStream, InputStreamReader, BufferedReader}
import java.net.Socket


/**
 * Created by nidev on 2015-01-09.
 */
class Session (sessionId : Integer, sock : Socket) extends Thread {
  // debug
  println("Session initiated : %d".format(sessionId))

  def finish : Unit = {
    sock.close
  }

  def isSockOkay : Boolean = {
    sock.isConnected && !sock.isClosed
  }

  def getSessionId : Integer = {
    sessionId
  }

  override def run() : Unit = {
    var readbuf = new BufferedReader(new InputStreamReader(new BufferedInputStream(sock.getInputStream)))
    sock.getOutputStream.write("Welcome to NetChat service\r\n".getBytes)
    println("수신된 데이터: %s".format(readbuf.readLine()))
    finish
  }



}
