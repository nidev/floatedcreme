package org.floatedcreme.protocols.traits

trait SSLRequired {
  val useSSL = true
  val SSLoptional = false 
}

trait SSLOptional {
  val useSSL = true
  val SSLoptional = true
  
}

trait PlainText {
  val useSSL = false
  val SSLoptional = false
}