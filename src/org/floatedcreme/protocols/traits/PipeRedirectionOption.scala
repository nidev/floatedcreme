package org.floatedcreme.protocols.traits

trait PipeRedirect {
  val usePipe = true
}

trait NoPipeRedirect {
  val usePipe = false
}

trait Conversation {
  val serverConversation = true;
}

trait NoConversation {
  val serverConversation = false;
}