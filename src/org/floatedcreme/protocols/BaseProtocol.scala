package org.floatedcreme.protocols

import scala.collection.mutable.HashMap

abstract class BaseProtocol(protocolName : String) {
  val protocolVersion = 0.1
  
  def toJSON : String
  def toDebugStr : String
  def fromJSON : Unit
  def setCommand(cmd : String) : Boolean
  def getCommand : String
  def loadParamsAsHash(map : HashMap[String, AnyRef]) : Boolean
  def loadParamsAsList(list : List[AnyRef]) : Boolean
  def getParamsPayload : Int  
}