package org.floatedcreme.protocols

import org.floatedcreme.protocols.traits.NoConversation
import org.floatedcreme.protocols.traits.PlainText
import org.floatedcreme.protocols.traits.NoPipeRedirect
import scala.collection.mutable.HashMap

class HelloWorld extends BaseProtocol("Hello World")
with NoConversation with PlainText with NoPipeRedirect {
  def toJSON : String = {
    ""
  }
  
  def toDebugStr : String = {
    ""
  }
  
  def fromJSON : Unit = {
    
  }
  
  def setCommand(cmd : String) : Boolean = {
    false
  }
  
  def getCommand : String = {
    ""
  }
  
  def loadParamsAsHash(map : HashMap[String, AnyRef]) : Boolean = {
    false
  }
  
  def loadParamsAsList(list : List[AnyRef]) : Boolean = {
    false
  }
  
  def getParamsPayload : Int = {
    0
  }  

}